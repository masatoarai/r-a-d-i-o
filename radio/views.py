from django.views.generic import View, TemplateView
from django.shortcuts import render
from django.db.models import Q

from stations.models import Station

class HomePageView(View):
    def get(self, request):
        titleQuery = request.GET.get('title')
        cityQuery = request.GET.get('city')

        query = Q()
        if titleQuery:
            query |= Q(title__icontains=titleQuery)
        stations = Station.objects.order_by('title').filter(query)


        return render(request, 'pages/home.html', {'stations': stations})

class AboutPageView(TemplateView):
    template_name = 'pages/about.html'

class SubmitPageView(TemplateView):
    template_name = 'pages/submit.html'
