from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic import TemplateView

from . import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.HomePageView.as_view(), name='home'),
    path('about/', views.AboutPageView.as_view(), name='about'),
    path('submit/', views.SubmitPageView.as_view(), name='submit'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
