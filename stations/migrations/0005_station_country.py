# Generated by Django 2.0.2 on 2018-06-21 22:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('categories', '0004_auto_20180621_2135'),
        ('stations', '0004_auto_20180621_2241'),
    ]

    operations = [
        migrations.AddField(
            model_name='station',
            name='country',
            field=models.ManyToManyField(blank=True, to='categories.Country'),
        ),
    ]
