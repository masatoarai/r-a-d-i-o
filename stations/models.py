from django.db import models

from categories.models import City, Country

class Station(models.Model):
    title = models.CharField(max_length=255)
    city = models.ManyToManyField('categories.City', blank=True)
    country = models.ManyToManyField('categories.Country', blank=True)
    image = models.ImageField(upload_to='images/')
    link = models.CharField(max_length=255, null=True)

    def __str__(self):
        return self.title
