from django.db import models

class City(models.Model):
    title = models.CharField(max_length=255)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = 'Cities'

class Country(models.Model):
    title = models.CharField(max_length=255)
    def __str__(self):
        return self.title
    class Meta:
        verbose_name_plural = 'Countries'
