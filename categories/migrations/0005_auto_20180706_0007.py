# Generated by Django 2.0.7 on 2018-07-06 00:07

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('stations', '0006_auto_20180706_0005'),
        ('categories', '0004_auto_20180621_2135'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='City',
            new_name='Cities',
        ),
    ]
